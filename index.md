---
title: Shotcrete automation
description: Use passive tracking of Bluetooth devices and Beacons to enhance construction
author: 
keywords: marp,marp-cli,slide
url: 
marp: true
image: 
---

# Shotcrete automation

---

<style>
img[alt~="center"], .center, .fence {
  display: block;
  margin: 0 auto;
}
kbd {
    background-color: #eee;
    border-radius: 3px;
    border: 1px solid #b4b4b4;
    box-shadow: 0 1px 1px rgba(0, 0, 0, .2), 0 2px 0 0 rgba(255, 255, 255, .7) inset;
    color: #333;
    display: inline-block;
    font-size: .85em;
    font-weight: 700;
    line-height: 1;
    padding: 2px 4px;
    white-space: nowrap;
}
.float-right {
    float: right;
}
.fence img {
    width: auto;
    max-width: 100%;
    height: auto;
    max-height: 100%;
}
.margin-top {
    margin-top: 1em;
}
.padding-top {
    padding-top: 1em;
}
</style>

# Collected stories

- _"Tunnel is dark and dust limits the visibility to few meters"_, said :construction_worker: on the :building_construction:
- _"Workers in tunnel are prone to danger of collapse"_, said 👨‍💼 on the :building_construction:
- _"The act of shortcrete is tedious under those conditions even though the task is simple"_, said :construction_worker: on the :building_construction:

---

# Mindmap

::: fence

@startuml

@startmindmap

*[#Orange] Operator constraint 
**[#Orange] Physical access
***[#Orange] Machinery
***[#Orange] Surroudings
***[#Orange] Controller
** Processing limits
*** Obstacle avoidance
**** Static
**** Dynamic
*** Road condition
**** Grip
**** Holes
*** Mechanics
**** Joint understanding
**** Acceleration control

@endmindmap

@enduml

:::

---

# Open-source & hardware secondary research

...

---

# Locked concept

...
